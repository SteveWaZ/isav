/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

/**
 *
 * @author SVOGEL
 */
public class UtilsString {
    
    public static String padBlank(int nb){
        String result = "";
        int i = 0;
        while(i < nb){
            result += " ";
            i++;
        }
        return result;
    }
    
    public static String padBlank(String text, int nb){
        String result = text;
        int i = result.length();
        while(i < nb){
            result += " ";
            i++;
        }
        return result;
    }
}
