/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import com.IseriesConnexion;
import com.ibm.as400.access.AS400Exception;
import com.ibm.as400.access.AS400SecurityException;
import com.ibm.as400.access.ErrorCompletingRequestException;
import com.ibm.as400.access.FTP;
import com.ibm.as400.access.ObjectDescription;
import com.ibm.as400.access.ObjectDoesNotExistException;
import com.ibm.as400.access.ObjectList;
import com.ibm.as400.access.RequestNotSupportedException;
import isav.Savf;
import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author SVOGEL
 */
public class UtilsObject {
    
    public static boolean checkObj(IseriesConnexion cnx, String lib, String obj, String type)
    {
        ObjectList ObjList = new ObjectList(cnx.getConnexion(), lib.toUpperCase(), obj.toUpperCase(), type.toUpperCase());
        boolean result = false;   
        try {
            // Object found
            result = ( ObjList.getLength() > 0 );
        } catch (AS400SecurityException | ErrorCompletingRequestException | InterruptedException | IOException | ObjectDoesNotExistException ex) {
            Logger.getLogger(Savf.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return result;
    }
    
    public static int getObjSize(IseriesConnexion cnx, String lib, String obj, String type)
    {
        int result = 0;   
        // Get Object list
        ObjectList ObjList = new ObjectList(cnx.getConnexion(), lib.toUpperCase(), obj.toUpperCase(), type.toUpperCase());
        try {
            try {                
                Enumeration curObject = ObjList.getObjects();                
                while( curObject.hasMoreElements() )
                {
                    // Get object description
                    ObjectDescription objDesc = (ObjectDescription) curObject.nextElement();             
                    result = Integer.valueOf(objDesc.getValueAsString(objDesc.OBJECT_SIZE));
                }
            } catch (AS400Exception | RequestNotSupportedException ex) {
                Logger.getLogger(UtilsObject.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (AS400SecurityException | ErrorCompletingRequestException | InterruptedException | IOException | ObjectDoesNotExistException ex) {
            Logger.getLogger(Savf.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return result;
    }
    
    public static boolean downloadFile(FTP ftp, String lib, String obj, File localFile){
        try {                                    
            // Upload            
            System.out.println("INFO : download in progress");
            if( !ftp.get(lib+"/"+obj, localFile) ){
                return false;
            } 
            //ftp.disconnect();
            
        } catch (IOException ex) {
            //@TODO : log that in file            
            System.out.println("ERROR : @TODO log => "+ ex.getMessage());
            return false;
            
        }               
        return true;
    }
}
