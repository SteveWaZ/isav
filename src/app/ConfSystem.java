/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConfSystem {   
    
    private static ConfSystem _Instance = null;
    private DatabaseSqlite db = DatabaseSqlite.getInstance();
        
    // Get instance of singleton
    public static ConfSystem getInstance() {
        
        if( _Instance == null ){
            _Instance = new ConfSystem();
        }        
        return _Instance;        
    }
     
    // Constructor
    public ConfSystem()
    {                        
    }   
    
    // Get All system
    public ResultSet getAll()
    {
        PreparedStatement pstmt = null;
        ResultSet result = null;
        try {
            String sql = "SELECT * FROM systems";            
            pstmt = db.getConnexion().prepareStatement(sql);
            result = pstmt.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(ConfSystem.class.getName()).log(Level.SEVERE, null, ex);
        }                
        
        return result;
    }
    // Get System by name
    public ResultSet get(String name)
    {
        PreparedStatement pstmt = null;
        ResultSet result = null;
        try {
            String sql = "SELECT * FROM systems WHERE name = ?";            
            pstmt = db.getConnexion().prepareStatement(sql);
            pstmt.setString(1, name);
            result = pstmt.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(ConfSystem.class.getName()).log(Level.SEVERE, null, ex);
        }                
        
        return result;
    }
    
    // Add new system without user
    public void add(String name, String ip)
    {
        add(name, ip, "", "");
    }
    // Add new system with user and password
    public void add(String name, String ip, String user, String pass)
    {
        PreparedStatement pstmt = null;
        try {                        
            String sql = "INSERT into systems ( name, ip, user, pass) VALUES (?,?,?,?)";
            pstmt = db.getConnexion().prepareStatement(sql);
            pstmt.setString(1, name);    
            pstmt.setString(2, ip);
            pstmt.setString(3, user);
            pstmt.setString(4, pass);
            
            pstmt.executeUpdate();
        } catch (SQLException ex){
            Logger.getLogger(ConfSystem.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
