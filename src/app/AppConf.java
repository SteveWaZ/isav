/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import java.io.File;


/**
 *
 * @author svogel
 * Singleton pour database
 */
public class AppConf {
   
    private String confDir = "";
    private String dataDir = "";
    private String appDirectory = "";
    public boolean initConf = false;
    public boolean initData = false;
    public static AppConf instance = null;
    
    
    public static AppConf getInstance(){
        if( instance == null ){
            instance = new AppConf();
        }        
        return instance;
    }
    
    /* Constructeur */
    public AppConf(){
        checkConfDir();    
        checkDataDir();
    }
    
    public void checkConfDir()
    {        
        // For Linux   : /Home/$User/.svsoft
        // For Windows : C:\Users\$User\.svsoft
        String curDir = System.getProperty("user.home")+"\\.svsoft";
        confDir = curDir.replace("\\", "/");
        File confDirectory = new File(confDir);
        if( confDirectory.exists() == false ){            
            initConf = true;
            confDirectory.mkdir();
        }
    }
               
    public String getConfDir(){
        return confDir;
    }
    
    public void checkDataDir(){        
        String curDir = System.getProperty("user.dir");
        dataDir = curDir.replace("\\", "/");
        File confDirectory = new File(dataDir);
        appDirectory = confDirectory.getParent();
        appDirectory = appDirectory.replace("\\", "/");
        
        if( confDirectory.exists() == false ){
            initData = true;
            confDirectory.mkdir();
        }
    }
               
    public String getDataDir(){
        return dataDir;
    }
    public String getAppDir(){
        return appDirectory;
    }
    
}
