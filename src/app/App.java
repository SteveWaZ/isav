/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import com.IseriesConnexion;
import java.io.Console;
import java.util.Scanner;

public class App {   
    
    private static App _Instance = null;    
    private String     currentLogin = "localhost@you";
    //public Scanner in = new Scanner(System.in);
    public Console in;
        
    public IseriesConnexion        cnxSrc      = null;
    public IseriesConnexion        cnxTgt      = null;
    
    
    // Get instance of singleton
    public static App getInstance() {
        
        if( _Instance == null ){
            _Instance = new App();
        }        
        return _Instance;        
    }

    public App() {
        // not work in debug mode
        this.in = System.console();        
    }
     
    
    public void resetCurrentLogin()
    {
        currentLogin = "localhost@you";
    }
    public void setCurrentLogin(String CurrentLogin)
    {
        currentLogin = CurrentLogin;
    }
    
    
    public String getCurrentLogin()
    {
        return currentLogin;
    }
    
}
