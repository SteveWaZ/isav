/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author svogel
 * Singleton pour database
 */
public class DatabaseSqlite {
    
    private AppConf conf = AppConf.getInstance();    
    private Statement db = null;
    private Connection conn = null;        
    private static DatabaseSqlite instance = null;
    
    public static DatabaseSqlite getInstance(){
        if( instance == null ){
            instance = new DatabaseSqlite();
        }        
        return instance;
    }
    
    /* Constructeur */
    public DatabaseSqlite(){        
        
    }
    
    public Connection getConnexion()
    {                
        if( conn == null ){                        
            try {
                // db parameters for user                
                String url = "jdbc:sqlite:" + conf.getConfDir() + "/conf.svs";
                File confDirectory = new File(conf.getConfDir() + "/conf.svs");
                // Create file if not exist
                if( confDirectory.exists() == false ){            
                    conf.initData = true;                    
                }
                
                // create a connection to the database
                conn = DriverManager.getConnection(url);              

                db = conn.createStatement();
                
                if( conf.initData ){                    
                    //loguer.log("Initialisation config utilisateur.");
                    conf.initData = false;
                    initTables();
                }
            } catch (SQLException e) {
                //System.out.println(e.getMessage());
            }
        }
        return conn;
    }
    
    public boolean disconnect ()
    {
        try
        {
            if(conn != null)
                conn.close();
             
            return true;
        }
        catch(SQLException e)
        {
            e.printStackTrace();
            return false;
        }
    }
    
    public ResultSet query(String requete)
    {
        getConnexion();
        try
        {
            return db.executeQuery(requete);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
         
        return null;
    }
     
    public void initTables(){
        // Table settings
        query("CREATE TABLE `systems` (" +                
                "`system` TEXT NOT NULL UNIQUE, " +
                "`ip` TEXT NOT NULL UNIQUE, " +                
                "`description` TEXT" +                
                ");");            
    }
   
    
}
