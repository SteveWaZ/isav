/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import java.util.HashMap;

public class AppArgs {   
    
    private static AppArgs _Instance = null;
    private boolean verbose = false;
    // map of app arguments
    private HashMap<String, String> arguments = null;          
    private String[]                arrayStringArguments = null;
    
    // Get instance of singleton
    public static AppArgs getInstance() {
        
        if( _Instance == null ){
            _Instance = new AppArgs();
        }        
        return _Instance;        
    }
     
    // Constructor
    public AppArgs()
    {                
        arguments = new HashMap<String, String>();
    }   
    
    public void setArgString(String[] ArrayString)
    {
        arrayStringArguments = ArrayString;
    }
    public void setParm(String key, String value)
    {
        if(key.equals("v")){
            verbose = true;
        }
        arguments.put(key, value);
    }
    public String getParm(String key)
    {
        return getParm(key, -1);
    }
    // Return parm in argument map
    public String getParm(String key, Integer id)
    {
        if( arguments.containsKey(key) )
        {
            return arguments.get(key);
        }
        
        if( id != -1 ){
            return getParmString(id);
        }
        
        return null;
    }
    
    private String getParmString(Integer id)
    {
        // Bound
        if( id > arrayStringArguments.length-1 )
            return null;
        
        return arrayStringArguments[id];        
    }
    
    
    public boolean parmExist(String key)
    {
        return arguments.containsKey(key);
    }
    public void reset()
    {
        arrayStringArguments = null;
        arguments.clear();
    }
    
    public boolean isVerbose()
    {
        return verbose;
    }
      
    
}
