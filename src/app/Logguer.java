/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Logguer {   
    
    private static Logguer _Instance = null;    
    private String path  = null;
    private File logMain = null;
    private File logDay  = null;
       
    // Get instance of singleton
    public static Logguer getInstance() {
        
        if( _Instance == null ){
            _Instance = new Logguer();
        }        
        return _Instance;        
    }
     
    
    public Logguer()
    {
        path = System.getProperty("user.dir");
        logMain = new File(path+"/log/log.txt");
        if( logMain.exists() == false ){
            logMain.mkdirs();
            try {
                logMain.createNewFile();
            } catch (IOException ex) {
                Logger.getLogger(Logguer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        SimpleDateFormat fDate = new SimpleDateFormat("yy/MM/dd - HH:mm:ss");        
        String fName = fDate.format(System.currentTimeMillis());
        
        logDay = new File(path+"/log/"+fName+".txt");                
    }
}

