/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isav;

import app.AppArgs;
import app.App;
import app.ConfSystem;
import app.DatabaseSqlite;
import com.IseriesConnexion;
import java.io.Console;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.UtilsString;

/**
 *
 * @author SVOGEL
 */
public class Systems {
    
    private DatabaseSqlite  db          = DatabaseSqlite.getInstance();
    private AppArgs         appArgs     = AppArgs.getInstance();    
    private App             app         = App.getInstance();            
    
    public void dispatch(String command)
    {
        if( command.toLowerCase().startsWith("connect") ){
            connect();
        }else
        if( command.toLowerCase().startsWith("close") || 
            command.toLowerCase().startsWith("bye")   ||
            command.toLowerCase().startsWith("signoff")            
            ){
            close();
        }else        
        if( command.toLowerCase().startsWith("list") ){
            list();
        }        
        
    }
    
    // connect to a system
    private void connect()
    {   
        if( appArgs.parmExist("help") ){
            connectHelp();
            return;
        }
        String ip     = appArgs.getParm("ip",  1);
        String user   = appArgs.getParm("usr", 2);
        String pass   = appArgs.getParm("pwd", 3);                
        
        if( ip == null ){
            System.out.println("ERROR : -ip missing");
            return;
        }        
        if( user == null ){
            System.out.println("ERROR : -usr missing");
            return;
        }
        if( pass == null ){
            System.out.print("password : ");                               
            pass = new String(app.in.readPassword());                        
            if( pass.trim().length() == 0 ){
                return;
            }
        }                
        
        // Connexion already exist
        if( app.cnxSrc != null ){
            app.cnxSrc.closeConnexion();
            System.out.println("INFO : connexion closed => " +app.cnxSrc.getIp());
            app.cnxSrc = null;
        }
        
        try {
            app.cnxSrc = new IseriesConnexion(ip);
            app.cnxSrc.validateLogin(user, pass);            
            System.out.println("INFO : connected => " + ip);
            app.setCurrentLogin(ip+"@"+user);
        } catch (Exception ex) {
            //@TODO : log that in file
            //Logger.getLogger(Systems.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("ERROR : connexion => " + ex.getMessage());
        }
    }
    private void connectHelp()
    {
        System.out.println();        
        System.out.println("----------------------------");
        System.out.println("-- Connect Help ------------");
        System.out.println(" -ip  : ip or dns of system");
        System.out.println(" -usr : user as/400");
        System.out.println(" -pwd : password as/400");     
        System.out.println("-- Sample ------------------");   
        System.out.println(" connect as.400.ip User ( your password will be ask )");     
        System.out.println(" connect as.400.ip User PasswordInClear");     
        System.out.println(" connect -ip as.400.ip -usr User -pwd PasswordInClear");     
        System.out.println();        
    }
    // Delete system
    public void close()
    {
        app.cnxSrc.closeConnexion();        
        app.resetCurrentLogin();
        
        System.out.println("INFO : disconnected => " + app.cnxSrc.getIp());
    }
    
    // edit
    public void edt()
    {
        System.out.println("edt");
    }
    
    // list of system
    public void list()
    {
        
        // 40 | 20 | 40
        System.out.println("|--------------------|----------|--------------------|");                                                
        System.out.println("|-------- IP --------|-- USR ---|------ STATUS ------|");                                                
        System.out.println("|--------------------|----------|--------------------|");                                                

        if( app.cnxSrc != null ){             
            System.out.print("|"+UtilsString.padBlank(app.cnxSrc.getIp(), 20));                
            System.out.print("|"+UtilsString.padBlank(app.cnxSrc.getUser(), 10));    
            if( app.cnxSrc.isConnected() ){
                System.out.print("|"+UtilsString.padBlank("connected", 20));                                
            }else{
                System.out.print("|"+UtilsString.padBlank("close", 20));                
            }         
            System.out.println("|");
        }else{            
            System.out.println("|                  NO SYSTEM AVAILLABLE              |");                                                
        }
        System.out.println("|--------------------|----------|--------------------|");                                                
        
//        
//        String name = appArgs.getParm("system");
//        PreparedStatement pstmt = null;
//        ResultSet result = getSystem(name);        
//        
//        try {
//            int i = 0;
//            while(result.next())
//            {
//                // Header
//                if( i == 0 ){
//                    System.out.println(" + List of system saved +");
//                    // 40 | 20 | 40
//                    System.out.println("|------------------------------|--------------------|----------------------------------------|");                                                
//                    System.out.println("|---------- NAME --------------|-------- IP --------|------------ DESCRIPTION ---------------|");                                                
//                    System.out.println("|------------------------------|--------------------|----------------------------------------|");                                                
//                }
//                System.out.print("|"+UtilsString.padBlank(result.getString("system"), 30));               
//                System.out.print("|"+UtilsString.padBlank(result.getString("ip"), 20));                
//                System.out.print("|"+UtilsString.padBlank(result.getString("description"), 40));                
//                System.out.println("|");
//                i++;
//            }
//            if( i == 0 ){                
//                System.out.println("                             NO SYSTEM IN LIST                                              |");                                                                
//            }
//            System.out.println("|------------------------------|--------------------|----------------------------------------|");                                                                
//        } catch (SQLException ex) {
//            Logger.getLogger(Systems.class.getName()).log(Level.SEVERE, null, ex);
//        }
                
    }
    
    public ResultSet getSystem(String name)
    {        
        PreparedStatement pstmt = null;
        ResultSet result = null;
        
        // Get All
        if( name == null )
        {
            try {
                String sql = "SELECT * FROM systems";            
                pstmt = db.getConnexion().prepareStatement(sql);                
                result = pstmt.executeQuery();
            } catch (SQLException ex) {
                Logger.getLogger(ConfSystem.class.getName()).log(Level.SEVERE, null, ex);
            }  
        }else
        {
            try {
                String sql = "SELECT * FROM systems WHERE system = ?";            
                pstmt = db.getConnexion().prepareStatement(sql);
                pstmt.setString(1, name);
                result = pstmt.executeQuery();
            } catch (SQLException ex) {
                Logger.getLogger(ConfSystem.class.getName()).log(Level.SEVERE, null, ex);
            }                            
        }
        
        return result;        
                
    }       
}
