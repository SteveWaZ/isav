/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isav;

/**
 *
 * @author SVOGEL
 */
public class CommandDispatcher {
    
    Systems system = null;
    Savf    savf   = null;
    // Dispatch following command needed
    public void dispatch(String command)
    {        
        if( command.toLowerCase().startsWith("connect") || 
            command.toLowerCase().startsWith("system")  ||
            command.toLowerCase().startsWith("bye")     ||
            command.toLowerCase().startsWith("close")   ||
            command.toLowerCase().startsWith("signoff" ) 
            ){
            if( system == null )
                system = new Systems();            
            system.dispatch(command);
            
            return;
        }else
        if( command.toLowerCase().startsWith("put") || 
            command.toLowerCase().startsWith("get") 
            ){
            if( savf == null )
                savf = new Savf();            
            savf.dispatch(command);
            
            return;
        }
        
        System.out.println("INFO : command not recognized");
    }
}
