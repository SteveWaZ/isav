
package isav;

import java.util.Scanner;
import app.AppArgs;
import app.App;




public class AppMain {
       
    private AppArgs appArgs = AppArgs.getInstance();
    private App     app     = App.getInstance();
    
    private CommandDispatcher dispatcher = null;
    private int nbCommand = 0;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {                
        
        AppMain app = new AppMain();        
        app.loop(args);               

        if( app.appArgs.isVerbose() ){            
            System.out.println("INFO : nombre command run = "+app.getNbCommand());
        }
        
        // Disconnect
        if( app.app.cnxSrc != null )
            app.app.cnxSrc.closeConnexion();
        if( app.app.cnxTgt != null )
            app.app.cnxTgt.closeConnexion();
    }
    
    
    public void browseArguments(String[] args)
    {
        appArgs.reset();
        appArgs.setArgString(args);
        
        if( args.length == 0 )
            return;
        
        appArgs.setParm("command", args[0]);
        
        String value = null;
        for(int i = 1; i < args.length; i++)
        {
            // Si argument avec "-"
            if( args[i].startsWith("-") )
            {
                if( i+1 >= args.length ){
                    value = null;
                }else
                if( args[i+1].startsWith("-") )
                    value = null;
                else
                    value = args[i+1];
                
                appArgs.setParm(args[i].replaceAll("-", ""), value);
            }
        }
    }
    
    // While if needed
    public void loop(String[] firstArgs)
    {
        String[] args = firstArgs;
        String command = null;
        
        showWelcome();
        browseArguments(args);        
        while(true)
        {                            
            System.out.print(app.getCurrentLogin()+": ");
            args = app.in.readLine().split(" ");
            
            // Command is alway first arg
            command = args[0];
            
            // Exit program
            if( command.toLowerCase().equals("exit") ){
                break;
            }else if( command.toLowerCase().equals("help") || command.toLowerCase().equals("-help") ){
                showHelp();
            }
            
            browseArguments(args);
            execCommand();
            
            nbCommand++;
        }
    }
    
    public void execCommand()
    {
//        if( appArgs.isVerbose() )
//            System.out.println("INFO : "+appArgs.getParm("command"));
        
        if( dispatcher == null )
            dispatcher = new CommandDispatcher();
        
        dispatcher.dispatch(appArgs.getParm("command"));
    }
    public int getNbCommand(){
        return nbCommand;
    }
    
    private void showWelcome()
    {
        System.out.println("----------------------------");
        System.out.println("----------- ISAV -----------");
        System.out.println("----------------------------");
        System.out.println("sav, push and pull your savf\r\n");
        System.out.println();
    }
    
    private void showHelp()
    {
        System.out.println();        
        System.out.println("----------------------------");
        System.out.println("-- ISav Help ---------------");
        System.out.println("Commands availlable :");                                
        System.out.println(" connect     : connect to an as/400");
        System.out.println(" close / bye : disconnect current connexion");
        System.out.println(" get         : download savf from AS/400 to local directory");
        System.out.println(" put         : upload local savf to AS/400");
        System.out.println(" -help       : help availlable for all commands");
        System.out.println(" -v          : verbose mode");        
        System.out.println();        
        
    }
    
    
}
