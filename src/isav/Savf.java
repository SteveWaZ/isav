/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isav;

import app.AppArgs;
import app.App;
import app.DatabaseSqlite;
import com.IseriesConnexion;
import com.ibm.as400.access.AS400SecurityException;
import com.ibm.as400.access.ErrorCompletingRequestException;
import com.ibm.as400.access.FTP;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.UtilsObject;

/**
 *
 * @author SVOGEL
 */
public class Savf {
    
    private DatabaseSqlite  db          = DatabaseSqlite.getInstance();
    private AppArgs         appArgs     = AppArgs.getInstance();    
    private App             app         = App.getInstance();    
            
    
    public void dispatch(String command)
    {
        if( command.toLowerCase().contains("put") ){
            put();
        }else
        if( command.toLowerCase().contains("get") ){
            get();
        }
            
        
        
    }
    // Put savf in 
    private void put()
    {
        if( appArgs.parmExist("help") ){
            putHelp();
            return;
        }
        // Get Parameters
        String path   = appArgs.getParm("path", 1);                       
        String savf   = appArgs.getParm("savf", 2);                       
        boolean force = appArgs.parmExist("f");                       
        String lib = null;
        String obj = null;
        // Check Parameters
        if( path == null ){
            System.out.println("ERROR : -path missing");
            return;
        }
        if( savf == null ){
            System.out.println("ERROR : -savf missing");
            return;
        }else{
            if( savf.contains("/") == false ){
                System.out.println("ERROR : -savf wrong ( format require : LIB/OBJ )");
                return;
            }else{
                String[] tmp = savf.split("/");
                lib = tmp[0];
                obj = tmp[1];
            }
        }
        
        // Open Local File
        File f = new File(path);
        if(f.exists() == false ){
            System.out.println("ERROR : local file not exist");           
            return; 
        }
        
        boolean exist = UtilsObject.checkObj(app.cnxSrc, lib, obj, "*FILE");
        // Savf already exist
        if( exist ){
            if( force == false ){
                System.out.println("ERROR : SAVF "+savf+" already exist ( add -f for overwrite it )");           
                return;                 
            }
            clearSavf(app.cnxSrc, lib, obj);
        }else{            
            createSavf(app.cnxSrc, lib, obj);
        }        
        
        if( !putSAVF(app.cnxSrc.getFtp(), lib, obj, f) ){
            System.out.println("ERROR : FTP error during upload");           
            return;
        }else{
            System.out.println("upload done");           
        }                   
    }
    
    
    private boolean putSAVF(FTP ftp, String lib, String obj, File localFile){
        try {                                    
            // Upload            
            System.out.println("INFO : upload in progress");
            ftp.setDataTransferType(FTP.BINARY);
            if( !ftp.put(localFile, lib+"/"+obj) ){
                return false;
            } 
            ftp.disconnect();
            
        } catch (IOException ex) {
            //@TODO : log that in file            
            System.out.println("ERROR : @TODO log => "+ ex.getMessage());
            //Logger.getLogger(TRANSFERTJ.class.getName()).log(Level.SEVERE, null, ex);
        }
                
        return true;
    }
    
    
    
    private void clearSavf(IseriesConnexion cnx, String lib, String obj)
    {
        if( appArgs.isVerbose() ){
            System.out.println("INFO : clear savf");
        }
        try {
            cnx.getCmd().run("CLRSAVF "+lib+"/"+obj);
        } catch (AS400SecurityException | ErrorCompletingRequestException | IOException | InterruptedException | PropertyVetoException ex) {
            Logger.getLogger(Savf.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void createSavf(IseriesConnexion cnx, String lib, String obj)
    {
        if( appArgs.isVerbose() ){
            System.out.println("INFO : create savf");
        }
        try {
            cnx.getCmd().run("CRTSAVF "+lib+"/"+obj);
        } catch (AS400SecurityException | ErrorCompletingRequestException | IOException | InterruptedException | PropertyVetoException ex) {
            Logger.getLogger(Savf.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    // Get savf in 
    private void get()
    {
        boolean exist = false;
        int     size  = 0;
        File    f     = null;
        // Get Parameters
        String savf   = appArgs.getParm("savf", 1);                       
        String path   = appArgs.getParm("path", 2);                       
        boolean force = appArgs.parmExist("f");                       
        String lib = null;
        String obj = null;
        // Check Parameters
        if( savf == null ){
            System.out.println("ERROR : -savf missing");
            return;
        }else{
            if( savf.contains("/") == false ){
                System.out.println("ERROR : -savf wrong ( format require : LIB/OBJ )");
                return;
            }else{
                String[] tmp = savf.split("/");
                lib = tmp[0];
                obj = tmp[1];
            }
        }
        if( path == null ){
            path = System.getProperty("user.dir");
            path += "/"+obj+".savf";
            System.out.println("INFO : local path => " + path);            
        }
        
        // Open Local File
        f = new File(path);
        if(f.exists() ){
            if( force == false ){
                System.out.println("ERROR : local file already exist ( add -f for overwrite it )");           
                return; 
            }
            f.delete();
            try {
                f.createNewFile();
            } catch (IOException ex) {
                // @TODO : log
                System.out.println("ERROR : on create local file");           
                Logger.getLogger(Savf.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        exist = UtilsObject.checkObj(app.cnxSrc, lib, obj, "*FILE");
        // Savf already exist
        if( exist == false){            
            System.out.println("ERROR : SAVF "+savf+" not exist");           
            return;                             
        }
        
        size = UtilsObject.getObjSize(app.cnxSrc, lib, obj, "*FILE");
        System.out.println("INFO : object size "+ size + " octets");
        
        if( !UtilsObject.downloadFile(app.cnxSrc.getFtp(), lib, obj, f) ){
            System.out.println("ERROR : FTP error during upload");
            return;
        }else{
            System.out.println("download done");           
        }
    }
    private void putHelp()
    {
        System.out.println();        
        System.out.println("----------------------------");
        System.out.println("-- Put Help ----------------");
        System.out.println(" -path : local path of your savf");
        System.out.println(" -savf : LIB/SAVF target");
        System.out.println(" -f    : (force) clear savf already exist on as/400 ");        
        System.out.println();        
    }
     
}