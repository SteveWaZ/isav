/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.AS400SecurityException;
import com.ibm.as400.access.CommandCall;
import com.ibm.as400.access.FTP;
import static com.ibm.as400.access.FTP.BINARY;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author svogel
 */
public class IseriesConnexion {   
    
    // CONNEXION
    private AS400       cnx         = null;                
    private CommandCall cmdCall     = null;    
    private FTP    ftp         = null;
    
    // ERREUR
    public boolean  error = false;      // Actuellement en erreur
    private String   errorMessage;       // Message ou exception
    private String   errorId;            // Erreur personalisé pour retour
    
    private String ip   = null;
    private String user = null;
    private String password = null;
    private boolean connected = false;
    
    // Construct
    public IseriesConnexion(String Ip) throws Exception
    {        
        ip = Ip;        
                
        cnx = new AS400(Ip);         
//        try {
//            cnx.setGuiAvailable(false);
//        } catch (PropertyVetoException ex) 
//        { 
//            throw new Exception("userLogin", ex);
//        }                                      
        
//        if( cnx.isConnected() == false ){
//            
//            throw new Exception("connexion");             
//        } 
 
        connected = true;
    }
    
    public boolean validateLogin(String User, String Password){
        user = User;      
        password = Password;
        connected = false;
        try {
            cnx.setUserId(User);
            cnx.setPassword(password);
            
            try {                
                connected = cnx.validateSignon();                                
                connected = true;
                
                //logManager.com("Connexion Iseries [OK]");                
            } catch (AS400SecurityException | IOException ex) {                
                //logManager.com("Connexion Iseries [ERREUR]");
                //logManager.com(ex.getMessage());
                connected = false;
                //@TODO : log
                System.out.println("@TODO : IseriesConnexion 1");
            }
        } catch (PropertyVetoException ex) {            
            connected = false;
        }
        
        if( connected ){            
            try {                
                // Command
                cmdCall = new CommandCall(cnx);
                
                
                // FTP                
                ftp = new FTP(ip, user, password);
                try {
                    ftp.connect();
                    ftp.setDataTransferType(BINARY);                    
                    ftp.disconnect();
                    ftp = null;
                } catch (IllegalStateException ex) {
                    Logger.getLogger(IseriesConnexion.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            } catch (IOException | IllegalStateException ex) {
                Logger.getLogger(IseriesConnexion.class.getName()).log(Level.SEVERE, null, ex);   
                //@TODO : log
                System.out.println("@TODO : IseriesConnexion 2");
            }  
        }
        return connected;
    }
    
    public AS400 getConnexion() {        
        return cnx;
    } 
    public CommandCall getCmd() {        
        return cmdCall;
    } 
    public boolean isConnected() {        
        return connected;
    }  
    public FTP getFtp(){        
        try {
            if( ftp == null || ftp.noop() == false )
            {
                try {
                    ftp = new FTP(ip, user, password);
                    ftp.setDataTransferType(FTP.BINARY);
                    ftp.connect();                    
                } catch (IOException | IllegalStateException ex) {
                    System.out.println("ERROR : @TODO : IseriesConnexion FTP");
                    Logger.getLogger(IseriesConnexion.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
        } catch (IOException ex) {
            System.out.println("ERROR : @TODO : IseriesConnexion catch noop FTP");
            Logger.getLogger(IseriesConnexion.class.getName()).log(Level.SEVERE, null, ex);                    
        }
        
        return ftp;
    }
    public void closeFtp(){        
        try {
            ftp.disconnect();
        } catch (IOException ex) {
            Logger.getLogger(IseriesConnexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        ftp = null;
    }
    

    public void closeConnexion() {
        cnx.disconnectAllServices();
    }       
    
    public String getIp()
    {
        return ip;
    }
    public String getUser()
    {
        return user;
    }
    
}
